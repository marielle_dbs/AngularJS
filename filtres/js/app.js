app = angular.module('app', []);

app.controller('CommentsCtrl', function($scope, $http, $filter){

	$scope.comments = []
	$scope.date = $filter('date')(12883236230006, 'medium')
	$scope.chiffre = 2.87466535 //$filter('round')(2.87466535, 2) et enlever 'round:4' dans index.html
	$scope.search = {}

	$http.get('http://jsonplaceholder.typicode.com/comments').then(function(response){
		$scope.comments = response.data
	})
})

app.filter('round', function(){
	return function(input, precision){
		chiffre = Math.round(input * Math.pow(10, precision)) / Math.pow(10, precision)
		return chiffre.toString().replace('.', ',')
	}
})
